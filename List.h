#ifndef _LIST_H_INCLUDE_
#define _LIST_H_INCLUDE_

// Exported reference type
typedef struct ListObj* List;

// constructor for the List type
List newList(void);

// destructor for the List type
void freeList(List* pL);

// Returns the number of elements in the List
int length(List L);

// returns the index of the cursor element
int index(List L);

// returns front element of List
int front(List L);

// returns back element of List
int back(List L);

// returns cursor element
int get(List L);

// returns true if this List and L are the same integer sequence
int equals(List A, List B);

// resets this List to its original empty state
void clear(List L);

// places cursor under front element of List
void moveFront(List L);

// places cursor under the back element of List
void moveBack(List L);

// moves cursor one step toward front of List unless cursor is undefined or at front of List
void movePrev(List L);

// moves cursor one step toward back of List unless cursor is undefined or at back of List
void moveNext(List L);

// inserts new element at front of List if List is empty, becomes first element in List 
void prepend(List L, int data);

// inserts new element at back of List if List is empty, becomes first element in List 
void append(List L, int data);

// inserts new element before cursor
void insertBefore(List L, int data);

// inserts new element after cursor
void insertAfter(List L, int data);

// deletes the front element 
void deleteFront(List L);

// deletes the back element
void deleteBack(List L);

// deletes cursor element, making cursor undefined
void delete(List L);

// prints List to out file
void printList(FILE* out, List L);

// returns a new List representing the same integer sequence as this List
List copyList(List L);

// returns a new List which is the concatenation of this list followed by L
List concatList(List A, List B);

#endif

